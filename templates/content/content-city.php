<?php
/**
	* Single post partial template
	*
	* @package Understrap
	*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	</header><!-- .entry-header -->

	<div class="row">
		<div class="col-4"><?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?></div>
		<div class="col-8"><?php the_content(); ?></div>
	</div>

	<section>
		<h3>
			<?php echo __( 'Последние 10 объектов недвижимости в городе', 'realty' ); ?>
		</h3>

		<div class="row">
			<?php
			$loop = new WP_Query( [
				'post_type'  => 'realty',
				'limit'      => 10,
				'per_page'   => 10,
				'meta_query' => [
					[
						'key'   => '_realty_city',
						'value' => get_the_ID(),
					]
				]
			] );
			foreach ( $loop->posts as $_post ) { ?>
				<div class="col-6">
					<div class="row">
						<div class="col-4 align-self-center p-1">
							<div class="image-fluid"><?php echo get_the_post_thumbnail( $_post->ID ); ?></div>
						</div>
						<div class="col-7">
							<div class="entry-content">
								<h3><a href='<?php echo esc_url( get_permalink( $_post->ID ) ); ?>'>
										<?php echo get_the_title( $_post->ID ); ?></a></h3>
								<?php realty_parameters( $_post->ID, "realty_city" ); ?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</section>

	<div class="entry-content">

		<?php
		understrap_link_pages();
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php understrap_entry_footer(); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
