<?php
/**
	* Single post partial template
	*
	* @package Understrap
	*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">

		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<section>
		<div class="row">
			<div class="col-5">
				<?php echo get_the_post_thumbnail( get_the_ID(), 'large' ); ?>
			</div>
			<div class="col-7">
				<?php realty_parameters( get_the_ID() ); ?>
			</div>
		</div>
	</section>
	<div class="entry-content">

		<?php
		the_content();
		if ( $images = get_post_meta( get_the_ID(), "_realty_photos", true ) ) {
			echo do_shortcode( '[gallery ids="' . $images . '"]' );
		}
		understrap_link_pages();
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php understrap_entry_footer(); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
