<section id="cities" class="style1">
	<h2><?php echo __( 'Города', 'realty' ); ?></h2>
	<?php

	$loop = new WP_Query( [
		'post_type' => 'realty_city',
		
	] );
	foreach ( $loop->posts as $_post ) {
		$r_in_city = new WP_Query( [
			'post_type'  => 'realty',
			'meta_query' => [
				[
					'key'   => '_realty_city',
					'value' => $_post->ID,
				]
			]
		] );
		?>
		<div class="row">
			<div class="col-4 align-self-center p-1"><?php echo get_the_post_thumbnail( $_post->ID ); ?></div>
			<div class="col-7">
				<h3><a href='<?php echo esc_url( get_permalink( $_post->ID ) ); ?>'>
						<?php echo get_the_title( $_post->ID ); ?></a></h3>
				<a href='<?php echo esc_url( get_permalink( $_post->ID ) ); ?>'>
					<?php echo sprintf( __( "%s объектов недвижимости", 'realty' ), $r_in_city->found_posts ); ?></a>
			</div>
		</div>
	<?php } ?>
</section>