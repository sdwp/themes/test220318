<section id="last-realty" class="style1">
<h2><?php echo __( 'Последние объекты недвижимости', 'realty' ); ?></h2>
	<?php
	$loop = new WP_Query( [
		'post_type' => 'realty',
	] );
	foreach ( $loop->posts as $_post ) { ?>
		<div class="row">
			<div class="col-4 align-self-center p-1"><div class="image-fluid"><?php echo get_the_post_thumbnail( $_post->ID ); ?></div></div>
			<div class="col-7">
				<div class="entry-content">
					<h3><a href='<?php echo esc_url( get_permalink( $_post->ID ) ); ?>'>
							<?php echo get_the_title( $_post->ID ); ?></a></h3>
					<?php realty_parameters( $_post->ID ); ?>
				</div>
			</div>
		</div>
	<?php } ?>
</section>