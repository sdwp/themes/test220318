<?php
/**
	* The main template file
	*
	* This is the most generic template file in a WordPress theme
	* and one of the two required files for a theme (the other being style.css).
	* It is used to display a page when nothing more specific matches a query.
	* E.g., it puts together the home page when no home.php file exists.
	* Learn more: http://codex.wordpress.org/Template_Hierarchy
	*
	* @package Understrap
	*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

	<div class="wrapper" id="index-wrapper">

		<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

			<div class="row">


				<main class="site-main" id="main">
					<div class="row">
						<div class="col-6">
							<?php get_template_part( 'templates/blocks/block', 'realty' ); ?>

						</div>
						<div class="col-6">
							<?php get_template_part( 'templates/blocks/block', 'cities' ); ?>
						</div>
					</div>

					<?php dynamic_sidebar( 'main-bottom-block' ); ?>
				</main><!-- #main -->

				<!-- The pagination component -->
				<?php understrap_pagination(); ?>

			</div><!-- .row -->

		</div><!-- #content -->

	</div><!-- #index-wrapper -->

<?php
get_footer();
